﻿using CoolParking.WebAPI.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ParkingController : ControllerBase
    {
        private readonly IParkingSessionService _parkingSessionService;

        public ParkingController(IParkingSessionService parkingSessionService)
        {
            _parkingSessionService = parkingSessionService;
        }

        // GET api/parking/balance
        [HttpGet("[action]")]
        public ActionResult<decimal> Balance()
        {
            return _parkingSessionService.GetBalance();
        }

        // GET api/parking/capacity
        [HttpGet("[action]")]
        public ActionResult<int> Capacity()
        {
            return _parkingSessionService.GetCapacity();
        }

        // GET api/parking/freePlaces
        [HttpGet("[action]")]
        public ActionResult<int> FreePlaces()
        {
            return _parkingSessionService.GetFreePlaces();
        }
    }
}
