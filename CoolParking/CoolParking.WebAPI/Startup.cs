using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.BL.Services;
using CoolParking.WebAPI.Interfaces;
using CoolParking.WebAPI.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace CoolParking.WebAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddSingleton<ILogService>(svc => new LogService(Configuration.GetValue<string>("LogPath")));
            services.AddSingleton<IParkingService>(svc => 
            {
                var withdrawTimer = new TimerService()
                {
                    Interval = Settings.WithdrawingPeriod
                };
                withdrawTimer.Start();

                var logTimer = new TimerService()
                {
                    Interval = Settings.LoggingPeriod
                };
                logTimer.Start();

                return new ParkingService(withdrawTimer, logTimer, svc.GetRequiredService<ILogService>());
            });
            services.AddSingleton<IParkingSessionService, ParkingSessionService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
