﻿using CoolParking.BL.Models;
using CoolParking.BL.Interfaces;
using CoolParking.WebAPI.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System;
using CoolParking.WebAPI.Models;

namespace CoolParking.WebAPI.Services
{
    public class ParkingSessionService : IParkingSessionService
    {
        private IParkingService _parkingService;

        public ParkingSessionService(IParkingService parkingService)
        {
            _parkingService = parkingService;
        }

        public void DeleteVehicle(string id)
        {
            _parkingService.RemoveVehicle(id);
        }

        public string GetAllTransactions()
        {
            return _parkingService.ReadFromLog();
        }

        public decimal GetBalance()
        {
            return _parkingService.GetBalance();
        }

        public int GetCapacity()
        {
            return _parkingService.GetCapacity();
        }

        public int GetFreePlaces()
        {
            return _parkingService.GetFreePlaces();
        }

        public TransactionInfo[] GetLastTransactions()
        {
            return _parkingService.GetLastParkingTransactions();
        }

        public Vehicle GetVehicle(string id)
        {
            var vehicle = _parkingService.GetVehicles().FirstOrDefault(x => x.Id == id) ??
                throw new ArgumentNullException("Vehicle with the specified ID is not parked");
            
            return vehicle;
        }

        public List<Vehicle> GetVehicles()
        {
            return new List<Vehicle>(_parkingService.GetVehicles());
        }

        public Vehicle PostVehicle(VehicleData vehicleData)
        {
            var vehicle = new Vehicle(vehicleData.Id, (VehicleType)vehicleData.VehicleType, (decimal)vehicleData.Balance);
            _parkingService.AddVehicle(vehicle);
            return vehicle;
        }

        public Vehicle TopUpVehicle(string id, decimal sum)
        {
            _parkingService.TopUpVehicle(id, sum);
            return GetVehicle(id);
        }
    }
}
