﻿// TODO: implement the ParkingService class from the IParkingService interface.
//       For try to add a vehicle on full parking InvalidOperationException should be thrown.
//       For try to remove vehicle with a negative balance (debt) InvalidOperationException should be thrown.
//       Other validation rules and constructor format went from tests.
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in ParkingServiceTests you can find the necessary constructor format and validation rules.

using CoolParking.BL.Exceptions;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Timers;

namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService, IDisposable
    {
        private readonly Parking parking;
        private readonly ITimerService _withdrawTimer;
        private readonly ITimerService _logTimer;
        private readonly ILogService _logService;
        private readonly Dictionary<VehicleType, decimal> tariffs;
        private List<TransactionInfo> BufferedParkingTransactions { get; } = new List<TransactionInfo>();

        public ParkingService(ITimerService withdrawTimer, ITimerService logTimer, ILogService logService)
        {
            parking = Parking.Instance;

            _withdrawTimer = withdrawTimer;
            _logTimer = logTimer;

            _withdrawTimer.Elapsed += WithdrawTimer_Elapsed;
            _logTimer.Elapsed += LogTimer_Elapsed;

            _logService = logService;

            tariffs = new Dictionary<VehicleType, decimal>()
            {
                { VehicleType.PassengerCar, Settings.PassengerCarTariff },
                { VehicleType.Truck, Settings.TruckTariff },
                { VehicleType.Bus, Settings.BusTariff },
                { VehicleType.Motorcycle, Settings.MotorcycleTariff }
            };
        }

        private void WithdrawTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            foreach (var vehicle in parking.Vehicles)
            {
                ExecuteTransaction(vehicle, GetAmount(vehicle));
            }
        }

        private void ExecuteTransaction(Vehicle vehicle, decimal amount)
        {
            vehicle.Balance -= amount;
            parking.Balance += amount;
            lock (BufferedParkingTransactions)
            {
                BufferedParkingTransactions.Add(new TransactionInfo(vehicle.Id, amount));
            }
        }

        private decimal GetAmount(Vehicle vehicle)
        {
            decimal initialBalance = vehicle.Balance;
            decimal tariff = tariffs[vehicle.VehicleType];
            decimal fine = Settings.FineCoefficient;

            if (vehicle.Balance < tariff)
            {
                if (initialBalance <= 0)
                {
                    return tariff * fine;
                }
                else
                {
                    return (tariff - initialBalance) * fine + initialBalance;
                }
            }
            else
            {
                return tariff;
            }
        }

        private void LogTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            var sb = new StringBuilder();
            TransactionInfo[] buffer;

            lock (BufferedParkingTransactions)
            {
                buffer = BufferedParkingTransactions.ToArray();
                BufferedParkingTransactions.Clear();
            }

            foreach (var transaction in buffer)
            {
                sb.Append($"[{transaction.Time}] ID: {transaction.Id} Amount: {transaction.Sum}\n");
            }

            _logService.Write(sb.ToString().TrimEnd('\n'));
        }

        public void AddVehicle(Vehicle vehicle)
        {
            if (isParked(vehicle))
            {
                throw new ArgumentException("Vehicle with the specified ID is already parked");
            }

            if (parking.Vehicles.Count < Settings.Capacity)
            {
                parking.Vehicles.Add(vehicle);
            }
            else
            {
                throw new InvalidOperationException("Parking is completely full");
            }
        }

        private bool isParked(Vehicle vehicle) => GetVehicle(vehicle.Id) is not null;

        public void Dispose()
        {
            _withdrawTimer.Elapsed -= WithdrawTimer_Elapsed;
            _logTimer.Elapsed -= LogTimer_Elapsed;

            parking.Vehicles.Clear();
            parking.Balance = 0.0M;
        }

        public decimal GetBalance() => parking.Balance;

        public int GetCapacity() => Settings.Capacity;

        public int GetFreePlaces() =>
            Settings.Capacity - parking.Vehicles.Count;

        public TransactionInfo[] GetLastParkingTransactions() =>
            BufferedParkingTransactions.ToArray();

        public ReadOnlyCollection<Vehicle> GetVehicles() =>
            new ReadOnlyCollection<Vehicle>(parking.Vehicles);

        public string ReadFromLog() => _logService.Read();

        public void RemoveVehicle(string id)
        {
            var vehicle = GetVehicle(id) ??
                throw new ArgumentException("Vehicle with the specified ID is not parked");

            if (vehicle.Balance < 0.0M)
            {
                throw new InvalidOperationException("Vehicle has a negative balance");
            }
            else
            {
                parking.Vehicles.Remove(vehicle);
            }
        }

        public void TopUpVehicle(string id, decimal sum)
        {
            var vehicle = GetVehicle(id) ??
                throw new ArgumentException("Vehicle with the specified ID is not parked");

            if (sum > 0.0M)
            {
                vehicle.Balance += sum;
            }
            else
            {
                throw new ArgumentException("The top-up amount must be greater than zero");
            }
        }

        public Vehicle GetVehicle(string id) =>
            parking.Vehicles.Find(i => i.Id == id);
    }
}